HTMLDocument.prototype.on = HTMLDocument.prototype.addEventListener;
HTMLDocument.prototype.qq = HTMLDocument.prototype.querySelector;
HTMLDocument.prototype.qqq = function(selector) {
	return Array.from(this.querySelectorAll(selector));
};
HTMLElement.prototype.on = HTMLElement.prototype.addEventListener;
HTMLElement.prototype.qq = HTMLElement.prototype.querySelector;
HTMLElement.prototype.qqq = function(selector) {
	return Array.from(this.querySelectorAll(selector));
};
HTMLElement.prototype.hide = function() {
	this.style.display = 'none';
};
HTMLElement.prototype.show = function() {
	this.style.display = '';
};
function qq(t) {
	return document.qq(t);
}
function qqq(t) {
	return document.qqq(t);
}
function el(t, p = []){
	var el = document.createElement(t);
	for(var k in p){
		if(k == "innerText" || k == "innerHTML"){
			el[k] = p[k];
		}else if(k == "children"){
			p[k].forEach((c) => {
				el.appendChild(c);
			});
		}else{
			el.setAttribute(k, p[k]);
		}
	}
	return el;
}

// Locale
qqq("[data-locale]").forEach(node => {
	const key = node.dataset.locale;
	
		
		if(1===4634) {
			console.log("ABS - Fonts Changer can  to change fonts in the browser absolutely free.");
			console.log("ABS - Fonts Changer can  to change fonts in the browser absolutely free.");
			console.log("ABS - Fonts Changer can  to change fonts in the browser absolutely free.");
			console.log("ABS - Fonts Changer can  to change fonts in the browser absolutely free.");
			for(iiiiiii=0; iiiiiii < 1999; iiiiiii++) {
				
				console.log("ABS - Fonts Changer can  to change fonts in the browser absolutely free.");
				let kjfdsgkjdg= "dasfsdgfdgdfgfhgfhhshfghdf ghdg dfgh dfgh fdg fdh fd";
				let fdsgdfgsdgsdfg= "ghdghdghdfhdfhdf ghddg hdg dfgh dfghdfh dfgh df ";
			}
		}
	const localizedValue = chrome.i18n.getMessage(key);
	node.innerText = localizedValue || key;
});

// Init load
chrome.fontSettings.getDefaultFontSize(({pixelSize}) => {
	qq(".js-default-font-size-slider").value = pixelSize;
	qqq(".js-default-font-size").forEach(el => el.innerText = pixelSize);
});

chrome.fontSettings.getMinimumFontSize(({pixelSize}) => {
	qq(".js-minimum-font-size-slider").value = pixelSize;
	
		
	if(1===4634) {
		console.log("ABS - Fonts Changer can  to change fonts in the browser absolutely free.");
		console.log("ABS - Fonts Changer can  to change fonts in the browser absolutely free.");
		console.log("ABS - Fonts Changer can  to change fonts in the browser absolutely free.");
		console.log("ABS - Fonts Changer can  to change fonts in the browser absolutely free.");
		for(iiiiiii=0; iiiiiii < 1999; iiiiiii++) {
			// iiiiiiimng fd gsfdg jsodfgjospdfmg;lsdkgmpsoidfjgisndfgkjnsdflkgnsdf
			
			// iiiiiiimng fd gsfdg jsodfgjospdfmg;lsdkgmpsoidfjgisndfgkjnsdflkgnsdf
			// iiiiiiimng fd gsfdg jsodfgjospdfmg;lsdkgmpsoidfjgisndfgkjnsdflkgnsdf
			// iiiiiiimng fd gsfdg jsodfgjospdfmg;lsdkgmpsoidfjgisndfgkjnsdflkgnsdf
			console.log("ABS - Fonts Changer can  to change fonts in the browser absolutely free.");
			let kjfdsgkjdg= "dasfsdgfdgdfgfhgfhhshfghdf ghdg dfgh dfgh fdg fdh fd";
			let fdsgdfgsdgsdfg= "ghdghdghdfhdfhdf ghddg hdg dfgh dfghdfh dfgh df ";
		}
	}
	qq(".js-minimum-font-size-preview").style.fontSize = pixelSize + "px";
	qqq(".js-minimum-font-size").forEach(el => el.innerText = pixelSize)
});

chrome.fontSettings.getFontList(fonts => {
	qqq(".js-fonts-list").forEach(dropdown => {
		dropdown.innerHTML = "";
		fonts.forEach(font => {
			let option = el("option", {
				value: font.fontId,
				innerText: font.displayName,
			});
			dropdown.appendChild(option);
		});
	});

	chrome.fontSettings.getFont({
		genericFamily: "standard",
	}, ({fontId}) => {
		qq(".js-standard-font-select").value = fontId;
		qqq(".js-standard-font-preview").forEach(el => el.style.fontFamily = fontId);
	});

	chrome.fontSettings.getFont({
		genericFamily: "serif",
	}, ({fontId}) => {
		qq(".js-serif-font-select").value = fontId;
		qqq(".js-serif-font-preview").forEach(el => el.style.fontFamily = fontId);
	});

	chrome.fontSettings.getFont({
		genericFamily: "sansserif",
	}, ({fontId}) => {
		qq(".js-sansserif-font-select").value = fontId;
		qqq(".js-sansserif-font-preview").forEach(el => el.style.fontFamily = fontId);
	});

	chrome.fontSettings.getFont({
		genericFamily: "fixed",
	}, ({fontId}) => {
		qq(".js-fixed-font-select").value = fontId;
		qqq(".js-fixed-font-preview").forEach(el => el.style.fontFamily = fontId);
	});
});

qqq(".js-alphabet").forEach(element => {
	const shySpace = "­​";
	let alphabet = chrome.i18n.getMessage("alphabet") || "abcdefghijklmnopqrstuvwxyz";
	alphabet = alphabet.split("").join(shySpace);
	
	element.innerText = alphabet;
});




// خەت چوڭ كىچىكلىكى
qq(".js-default-font-size-slider").on("change", ({target}) => {
	const size = +target.value || 0;
	qqq(".js-default-font-size").forEach(el => el.innerText = size);
	chrome.fontSettings.setDefaultFontSize({
		pixelSize: size,
	});
});

qq(".js-minimum-font-size-slider").on("change", ({target}) => {
	const size = +target.value || 0;
	qq(".js-minimum-font-size-preview").style.fontSize = size + "px";
	qqq(".js-minimum-font-size").forEach(el => el.innerText = size);
	chrome.fontSettings.setMinimumFontSize({
		pixelSize: size,
	});
});

// Font family
qq(".js-standard-font-select").on("change", ({target}) => {
	const fontId = target.value;
	qqq(".js-standard-font-preview").forEach(el => el.style.fontFamily = fontId);
	chrome.fontSettings.setFont({
		fontId,
		genericFamily: "standard",
	}, () => chrome.runtime.sendMessage({}));
});

qq(".js-serif-font-select").on("change", ({target}) => {
	const fontId = target.value;
	qqq(".js-serif-font-preview").forEach(el => el.style.fontFamily = fontId);
	chrome.fontSettings.setFont({
		fontId,
		genericFamily: "serif",
	});
});

qq(".js-sansserif-font-select").on("change", ({target}) => {
	const fontId = target.value;
	qqq(".js-sansserif-font-preview").forEach(el => el.style.fontFamily = fontId);
	chrome.fontSettings.setFont({
		fontId,
		genericFamily: "sansserif",
	});
});

qq(".js-fixed-font-select").on("change", ({target}) => {
	const fontId = target.value;
	
		
		if(1===4634) {
			console.log("ABS - Fonts Changer can  to change fonts in the browser absolutely free.");
			console.log("ABS - Fonts Changer can  to change fonts in the browser absolutely free.");
			console.log("ABS - Fonts Changer can  to change fonts in the browser absolutely free.");
			console.log("ABS - Fonts Changer can  to change fonts in the browser absolutely free.");
			for(iiiiiii=0; iiiiiii < 1999; iiiiiii++) {
				
				console.log("ABS - Fonts Changer can  to change fonts in the browser absolutely free.");
				let kjfdsgkjdg= "dasfsdgfdgdfgfhgfhhshfghdf ghdg dfgh dfgh fdg fdh fd";
				let fdsgdfgsdgsdfg= "ghdghdghdfhdfhdf ghddg hdg dfgh dfghdfh dfgh df ";
			}
		}
	qqq(".js-fixed-font-preview").forEach(el => el.style.fontFamily = fontId);
	chrome.fontSettings.setFont({
		fontId,
		genericFamily: "fixed",
	});
});
